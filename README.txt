
Egg: Worm Map is a puzzle free game app for the Android phone. This app lets you try to collect a bunch of worm by moving in a straight line and never going diagonal.

At some point everyone has seen one of the puzzles where you have to draw a shape without crossing any lines or picking up the pencil or the similar version where you must draw a line through each line in an image without crossing itself or any of them twice. Egg: worm map is similar to those game, but different at the same time, which makes it both familiar and fresh which is not something that is easy to achieve and that alone makes it worth trying, but there is more to this game.

When you open this game you will begin by having a map of worms and your character (egg) standing on some where. To move you simply swipe in the direction that you want to go and the egg will jump that way. There is no distance issues in this game only straight lines and not going diagonal. In each level you must try to land on each of the worm without finding yourself in a place where you can’t go to one or having to go diagonal. This is more difficult than in sounds.

The graphics and sound of this app are simple but very effective.  You can listen to your own music while using this app. The controls of this app are everything they need to be for a game that is as simple as this.

Game play:

	simple—swipe your finger vertically or horizontally across the screen to jump on each spot once, and only once (spots disappear after you jump off of them). You can leap across any amount of space, so long as you’re not going diagonal. Hit every spot once to clear the map and advance to the next level.

Feauture:
	- Endless levels and gameplay
	- Play more to get more challenge
	- Simple controls for all ages
	- Minimalist graphics for maximum appeal
	- Unique cerebral game play complimented
	- Surprisingly addictive!



