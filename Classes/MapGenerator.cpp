#include "MapGenerator.h"

MapGenerator::MapGenerator(int x, int y, int number)
{
	this->maxX = x;
	this->maxY = y;
	this->number = number;
	genStep = StepGenerator(maxX, maxY);
	srand((int)time(0));
	currentX = rand()%x;
	srand((int)time(0));
	currentY = rand()%y;
    for (int i = 0; i < 30; i++)
	{
        for (int j = 0; j < 30; j++)
		{
			map[i][j] = 0;
		}
	}
    heroX=currentX;
    heroY=currentY;
    map[currentX][currentY] = 1;
    previous=10;
}

int MapGenerator::checkExist(int x, int y)
{
	if(map[x][y]==1)
		return 1;
	return 0;
}

int MapGenerator::checkRow(int p1, int p2, int c)
{
    /*for (int i = p1; i < p2; i++)
        if (map[i][c]==1)
			return 1;
    */
	return 0;
}

int MapGenerator::checkCol(int p1, int p2, int r)
{
    /*for (int i = p1; i < p2; i++)
        if (map[r][i]==1)
			return 1;
    */
	return 0;
}

void MapGenerator::genMap()
{
	int i = 0, tempX, tempY;
    while (i < number)
	{
		tempX=currentX;
		tempY=currentY;
		genStep.nextStep(currentX, currentY);
		int direct = genStep.getDirect();
		int distance = genStep.getDistance();
		if (distance == 0)
		{
			continue;
		}
		switch (direct)
		{
		case StepGenerator::UP:
			tempY=currentY - distance;
            if (checkCol(tempY, currentY, currentX) == 1)
			{
				continue;
			}
            if  (previous==StepGenerator::DOWN)
            {
                continue;
            }
			break;
		case StepGenerator::DOWN:
			tempY=currentY + distance;
			if (checkCol(currentY+1, tempY, currentX) == 1)
			{
				continue;
			}
            if  (previous==StepGenerator::UP)
            {
                continue;
            }
			break;
		case StepGenerator::LEFT:
			tempX=currentX - distance;
            if (checkRow(tempX, currentX, currentY) == 1)
			{
				continue;
			}
            if  (previous==StepGenerator::RIGHT)
            {
                continue;
            }
			break;
		case StepGenerator::RIGHT:
			tempX=currentX + distance;
			if (checkRow(currentX+1, tempX, currentY) == 1)
            {
                continue;
            }
            if  (previous==StepGenerator::LEFT)
            {
                continue;
            }
			break;
		default:
			break;
		}
		if (checkExist(tempX, tempY) == 1)
			continue;
        if ((currentX!=tempX) && currentY!=tempY)
            continue;
		currentX = tempX;
		currentY = tempY;
        previous=direct;
		map[currentX][currentY] = 1;
        this->directions.push(direct);
		i += 1;
	}
}

MapGenerator::MapGenerator()
{

}

MapGenerator::~MapGenerator()
{

}
