#ifndef  _EGG_MONSTER_H_
#define  _EGG_MONSTER_H_

#include "cocos2d.h"
#include "StepGenerator.h"

USING_NS_CC;

class EggMonster: public Sprite
{
private:
    Animation* idleAnimation;
    Animation* rightAnimation;
    Animation* leftAnimation;
    Animation* downAnimation;
    Animation* backAnimation;
    Animation* rightIdle;
    Animation* leftIdle;
    Animation* upIdle;

    //frame vectors
    Vector<SpriteFrame* > idleFrames;
    Vector<SpriteFrame* > rightFrames;
    Vector<SpriteFrame* > leftFrames;
    Vector<SpriteFrame* > downFrames;
    Vector<SpriteFrame* > backFrames;
    Vector<SpriteFrame* > rightIdleFrames;
    Vector<SpriteFrame* > leftIdleFrames;
    Vector<SpriteFrame* > upIdleFrames;
    //Actions

    FiniteTimeAction* rightAction;
    FiniteTimeAction* leftAction;
    FiniteTimeAction* downAction;
    FiniteTimeAction* backAction;
    FiniteTimeAction* idleUpAction;
    FiniteTimeAction* idleLeftAction;
    FiniteTimeAction* idleRightAction;
public:
    int currentDirection;
    static SpriteBatchNode* spritebatch;
    static SpriteFrameCache* cache;
    static const std::string EGG_SS_PLIST;
    static const std::string EGG_SS_PNG;
    static const int ACTION_TAG;
    static const int IDLE_TAG;
    Sequence* seq;
	EggMonster();
	~EggMonster();
    static EggMonster* create();
    void initEverything();
    void runIdle();
    void runRight(Vec2 des);
    void runLeft(Vec2 des);
    void runDown(Vec2 des);
    void runBack(Vec2 des);

    void idleRightRun();
    void idleLeftRun();
    void idleUpRun();
    void currentIdle();

};

#endif
