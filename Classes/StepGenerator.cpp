#include "StepGenerator.h"

StepGenerator::StepGenerator()
{

}

StepGenerator::StepGenerator(int argMaxX, int argMaxY)
{
	this->maxX = argMaxX;
	this->maxY = argMaxY;
    struct timeval t1;
    gettimeofday(&t1, NULL);
    srand(t1.tv_usec*t1.tv_sec);
}

int StepGenerator::getDirect()
{
	return this->direct;
}

int StepGenerator::getDistance()
{
	return this->distance;
}

void StepGenerator::nextStep(int currentX, int currentY)
{
    direct = rand() % 80 + 1;
    direct %=4;
	int temp;
	switch (direct)
	{
	case UP:
		if (currentY < 1)
		{
			distance = 0;
			return;
		}
		//srand(t1.tv_usec*t1.tv_sec);
		distance = rand() % currentY + 1;
		break;
	case DOWN:
		if (maxY - currentY - 1 < 1)
		{
			distance = 0;
			return;
		}
		temp = maxY - currentY - 1;
		//srand(t1.tv_usec*t1.tv_sec);
		distance = rand() % temp + 1;
		break;
	case LEFT:
		if (currentX < 1)
		{
			distance = 0;
			return;
		}
		//srand(t1.tv_usec*t1.tv_sec);
		distance = rand() % currentX + 1;
		break;
	case RIGHT:
		if (maxX - currentX - 1 < 1)
		{
			distance = 0;
			return;
		}
		temp = maxX - currentX - 1;
		//srand(t1.tv_usec*t1.tv_sec);
		distance = rand() % temp + 1;
		break;
	default:
		break;
	}
}

StepGenerator::~StepGenerator()
{
}
