#include "HelloWorldScene.h"
#include <stdio.h>

USING_NS_CC;
using namespace cocostudio;
using namespace ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    MyBodyParser::getInstance()->parseJsonFile("egg_game.json");
    // Init worm animation
    background = CSLoader::createNode("Background.csb"); //create Node
    background->setTag(8080);
    this->addChild(background,-1); //add Node to Parent
    initAnimation();
    visibleSize = Director::getInstance()->getVisibleSize();
    this->origin = Director::getInstance()->getVisibleOrigin();
    this->actualHeight=visibleSize.height*0.8;
    auto tempMonster = Sprite::create("pikachu.png");
    this->sizeOfSquare = visibleSize.width / 9.0;
    this->firstX = this->sizeOfSquare / 2;
    this->firstY = visibleSize.height*0.12;
    float xScale = this->sizeOfSquare / tempMonster->getContentSize().width;
    this->sizeOfSquare = actualHeight / 16.0;
    float yScale = this->sizeOfSquare / tempMonster->getContentSize().height;
    this->scaleRate=xScale>yScale?yScale:xScale;
    this->sizeOfSquare=xScale<yScale?visibleSize.width / 9.0 : actualHeight / 16.0;
    this->firstX+=(visibleSize.width-9*this->sizeOfSquare)/2;
    //  Create a "one by one" touch event listener
    // (processes one touch at a time)
    this->readDataLevel();
    labelInit();
    lockSwipe=false;
    createMap(this->number,false);

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);

    listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(HelloWorld::onTouchCancelled, this);

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
    isTouchDown = false;
    AdmobHelper::showAd();
    initialTouchPos[0] = 0;
    initialTouchPos[1] = 0;
    showGuide();
    this->scheduleUpdate();
    return true;
}

void HelloWorld::createMap(int max, bool resote)
{
    if(getChildByTag(3128))
        getChildByTag(3128)->removeFromParentAndCleanup(true);

    tmpNum=max;
    levelLabel->setString("Level: "+intConvert(this->level));
    playTimeLabel->setString("Solved Map: "+intConvert(this->timePlayed)+"/"+intConvert(number>=15?10:1));
    //this->removeChildByTag(2011);
    this->mg = MapGenerator(16, 9, max);
    this->mg.genMap();
    /*debug
    std::string hehe="";
    while(mg.directions.empty()!=true)
    {
        hehe+=intConvert(mg.directions.front());
        mg.directions.pop();
    }
    auto templabel = Label::createWithTTF("Here: "+hehe,"fonts/Marker Felt.ttf",32);
    templabel->setPosition(Point(visibleSize.width/2,visibleSize.height*0.65));
    templabel->setColor(Color3B(255,215,0));
    templabel->setTag(3128);
    this->addChild(templabel,10);
    debug end*/
    if(resote==true)
        restoreMap();
    saveMap();
    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            if (mg.map[i][j] == 1)
            {
                if((i==mg.heroX) && (j==mg.heroY))
                   continue;
                auto bubbleMonster = Sprite::createWithSpriteFrameName("idle_frame_1.png");
                bubbleMonster->setPosition(Vec2(origin.x+firstX+j*sizeOfSquare,origin.y+firstY+i*sizeOfSquare));
                auto worm_body = MyBodyParser::getInstance()->bodyFormJson(bubbleMonster,"worm");
                bubbleMonster->setPhysicsBody(worm_body);
                worm_body->setContactTestBitmask(0x01);
                worm_body->setCollisionBitmask(0x01);
                worm_body->setDynamic(false);
                worm_body->setMass(0);
                worm_body->setGravityEnable(false);
                bubbleMonster->setScale(this->scaleRate);
				auto idleAction=RepeatForever::create(Animate::create(idleWormAnimation));
                bubbleMonster->runAction(idleAction);
                idleAction->retain();
                this->addChild(bubbleMonster,1,hashTag(i,j));
            }
        }
    }
    if(eggMonster==nullptr)
    {
        eggMonster=EggMonster::create();
        eggMonster->initEverything();
        this->addChild(eggMonster,2,2011);
        eggMonster->retain();
    }
    auto egg_body = MyBodyParser::getInstance()->bodyFormJson(eggMonster,"egg");
    egg_body->setMass(0);
    eggMonster->setPosition(Vec2(origin.x+firstX+mg.heroY*sizeOfSquare,origin.y+firstY+mg.heroX*sizeOfSquare));
    eggMonster->setPhysicsBody(egg_body);
    egg_body->setContactTestBitmask(0x01);
    egg_body->setCollisionBitmask(0x01);
    egg_body->setDynamic(false);
    eggMonster->setScale(this->scaleRate);
    eggMonster->currentDirection=100;
    eggMonster->runIdle();
    isIdle=true;
    gameState=true;
}

void HelloWorld::initAnimation()
{

    wormBatch = SpriteBatchNode::create("worm.png");
    wormCache = SpriteFrameCache::getInstance();
    wormCache->addSpriteFramesWithFile("worm.plist");

    char str[100]={0};
    for(int i = 1; i <= 10; i++)
    {
        sprintf(str, "idle_frame_%d.png", i);
        SpriteFrame* frame = wormCache->getSpriteFrameByName( str );
        idleWormFrames.pushBack(frame);
    }

    for(int i = 1; i <= 6; i++)
    {
        sprintf(str, "stomp_frame_%d.png", i);
        SpriteFrame* frame = wormCache->getSpriteFrameByName( str );
        stompWormFrames.pushBack(frame);
    }
    idleWormAnimation=Animation::createWithSpriteFrames(idleWormFrames, 0.1f);
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::createMonster(int tag, Vec2 position)
{
    auto spriteSheet=SpriteBatchNode::create("monster.png");
    this->addChild(spriteSheet);
    Vector<SpriteFrame*> aniframe(15);
    char str[50]={0};
    for(int i=0;i<5;i++){
        sprintf(str,"monster%d.png",i+1);
    }

    auto frame=SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
    aniframe.pushBack(frame);
    auto animation=Animation::createWithSpriteFrames(aniframe,0.1f);
    auto monster=Sprite::createWithSpriteFrameName("monster1.png");
    monster->setPosition(position);
    monster->setScale(0.25);
    auto idleAction=RepeatForever::create(Animate::create(animation));
    idleAction->retain();
    spriteSheet->addChild(monster);
    monster->runAction(idleAction);
}


void HelloWorld::update(float dt)
{
    if(eggMonster->numberOfRunningActions()==0)
        eggMonster->currentIdle();
    stompWormAnimation=Animation::createWithSpriteFrames(stompWormFrames, 0.3f);
    FiniteTimeAction* fadeAction=Repeat::create( Animate::create(stompWormAnimation),1);
    if (true == isTouchDown)
    {
        if(lockSwipe==true)
          return;
        newPos=-1;
        if (initialTouchPos[0] - currentTouchPos[0] > visibleSize.width * 0.05)
        {
            CCLOG("SWIPED LEFT");
            if(eggMonster->currentDirection==StepGenerator::RIGHT)
                return;
            newPos=searchPath(StepGenerator::LEFT);
            if(newPos!=-1)
            {
                lockSwipe=true;
                getChildByTag(hashTag(mg.heroX,mg.heroY))->getPhysicsBody()->setDynamic(true);
                eggMonster->runLeft(Vec2(origin.x+firstX+mg.heroY*sizeOfSquare,origin.y+firstY+mg.heroX*sizeOfSquare));
                CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
                "jump.wav");
                eggMonster->currentDirection=StepGenerator::LEFT;
                //getChildByTag(hashTag(mg.heroX,mg.heroY))->runAction(fadeAction);
            }
            isTouchDown = false;
        }
        else if (initialTouchPos[0] - currentTouchPos[0] < - visibleSize.width * 0.05)
        {
            CCLOG("SWIPED RIGHT");
            if(eggMonster->currentDirection==StepGenerator::LEFT)
                return;
            newPos=searchPath(StepGenerator::RIGHT);
            if(newPos!=-1)
            {
                lockSwipe=true;
                getChildByTag(hashTag(mg.heroX,mg.heroY))->getPhysicsBody()->setDynamic(true);
                eggMonster->runRight(Vec2(origin.x+firstX+mg.heroY*sizeOfSquare,origin.y+firstY+mg.heroX*sizeOfSquare));
                CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
                "jump.wav");
                eggMonster->currentDirection=StepGenerator::RIGHT;
                //getChildByTag(hashTag(mg.heroX,mg.heroY))->runAction(fadeAction);
            }
            isTouchDown = false;
        }
        else if (initialTouchPos[1] - currentTouchPos[1] > visibleSize.width * 0.05)
        {
            CCLOG("SWIPED UP");
            if(eggMonster->currentDirection==StepGenerator::DOWN)
                return;
            newPos=searchPath(StepGenerator::UP);
            if(newPos!=-1)
            {
                lockSwipe=true;
                getChildByTag(hashTag(mg.heroX,mg.heroY))->getPhysicsBody()->setDynamic(true);
                eggMonster->runBack(Vec2(origin.x+firstX+mg.heroY*sizeOfSquare,origin.y+firstY+mg.heroX*sizeOfSquare));
                CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
                "jump.wav");
                eggMonster->currentDirection=StepGenerator::UP;
                //getChildByTag(hashTag(mg.heroX,mg.heroY))->runAction(fadeAction);
            }
            isTouchDown = false;
        }
        else if (initialTouchPos[1] - currentTouchPos[1] < - visibleSize.width * 0.05)
        {
            CCLOG("SWIPED DOWN");
            if(eggMonster->currentDirection==StepGenerator::UP)
                return;
            newPos=searchPath(StepGenerator::DOWN);
            if(newPos!=-1)
            {
                lockSwipe=true;
                getChildByTag(hashTag(mg.heroX,mg.heroY))->getPhysicsBody()->setDynamic(true);
                eggMonster->runDown(Vec2(origin.x+firstX+mg.heroY*sizeOfSquare,origin.y+firstY+mg.heroX*sizeOfSquare));
                CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
                "jump.wav");
                eggMonster->currentDirection=StepGenerator::DOWN;
                //getChildByTag(hashTag(mg.heroX,mg.heroY))->runAction(fadeAction);

            }
            isTouchDown = false;
        }
    }

    if(newPos!=-1 && eggMonster->getActionByTag(EggMonster::ACTION_TAG)==NULL)
    {
        if(isIdle==false)
        {
            lockSwipe=false;
            isIdle=true;
            if(eggMonster->numberOfRunningActions()==0)
                eggMonster->currentIdle();
            if(isLose()==true)
            {                
                loseCase();
            }
        }
        auto deadBuble=this->getChildByTag(hashTag(mg.heroX,mg.heroY));
        if(deadBuble!=nullptr)
        {
            deadBuble->stopAllActions();
            deadBuble->removeFromParentAndCleanup(true);
            tmpNum-=1;
            isIdle=false;
        }
    }
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *event)
{
    initialTouchPos[0] = touch->getLocation().x;
    initialTouchPos[1] = touch->getLocation().y;
    currentTouchPos[0] = touch->getLocation().x;
    currentTouchPos[1] = touch->getLocation().y;
    isTouchDown = true;

    return true;
}

void HelloWorld::onTouchMoved(Touch *touch, Event *event)
{
    currentTouchPos[0] = touch->getLocation().x;
    currentTouchPos[1] = touch->getLocation().y;
}

void HelloWorld::onTouchEnded(Touch *touch, Event *event)
{
    isTouchDown = false;
    if(gameState==false)
    {
        getChildByTag(1102)->removeFromParentAndCleanup(true);
        getChildByTag(1103)->removeFromParentAndCleanup(true);
        getChildByTag(1104)->removeFromParentAndCleanup(true);
        createMap(this->number,false);
    }
}

void HelloWorld::onTouchCancelled(Touch *touch, Event *event)
{
    onTouchEnded(touch, event);
}


int HelloWorld::searchPath(int direct)
{
    int i=-1;
    switch(direct)
    {
    case StepGenerator::RIGHT:
        for(i=mg.heroY+1; i<mg.getMaxY(); i++)
        {
            if(mg.checkExist(mg.heroX,i)==1)
                break;
        }

        if(mg.checkExist(mg.heroX,i)==1)
        {
            mg.map[mg.heroX][mg.heroY]=0;
            mg.heroY=i;
        }else
        {
            i=-1;
        }
        break;
    case StepGenerator::LEFT:
        for(i=mg.heroY-1; i>=0; i--)
        {
            if(mg.checkExist(mg.heroX,i)==1)
                break;
        }

        if(mg.checkExist(mg.heroX,i)==1)
        {
            mg.map[mg.heroX][mg.heroY]=0;
            mg.heroY=i;
        }
        else
        {
            i=-1;
        }
        break;
    case StepGenerator::UP:
        for(i=mg.heroX-1; i>=0; i--)
        {
            if(mg.checkExist(i,mg.heroY)==1)
                break;
        }

        if(mg.checkExist(i,mg.heroY)==1)
        {
            mg.map[mg.heroX][mg.heroY]=0;
            mg.heroX=i;
        }
        else
        {
            i=-1;
        }
        break;
    case StepGenerator::DOWN:
        for(i=mg.heroX+1; i<mg.getMaxX(); i++)
        {
            if(mg.checkExist(i,mg.heroY)==1)
                break;
        }

        if(mg.checkExist(i,mg.heroY)==1)
        {
            mg.map[mg.heroX][mg.heroY]=0;
            mg.heroX=i;
        }
        else
        {
            i=-1;
        }

        break;
    default:
        break;
    }
    return i;
}

bool HelloWorld::isLose()
{
    int startx=0;
    int starty=0;
    int endx=mg.getMaxX();
    int endy=mg.getMaxY();
    switch(eggMonster->currentDirection)
    {
        case StepGenerator::UP:
            endx=mg.heroX;
            break;
        case StepGenerator::DOWN:
            startx=mg.heroX+1;
            break;
        case StepGenerator::LEFT:
            endy=mg.heroY;
            break;
        case StepGenerator::RIGHT:
            starty=mg.heroY+1;
            break;
    }

    for(int i=startx; i<endx; i++)
    {
        if(i!=mg.heroX && mg.map[i][mg.heroY] == 1)
        {
            return false;
        }
    }
    for(int i=starty; i<endy; i++)
    {
        if(i!=mg.heroY && mg.map[mg.heroX][i] == 1)
        {
            return false;
        }
    }
    if(tmpNum == 0)
    {
        eggMonster->currentDirection=StepGenerator::UP;
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
        "win.wav");
        auto label = Label::createWithTTF("VICTORY","fonts/Mu online.ttf",64);
        label->setPosition(Point(visibleSize.width/2,visibleSize.height*0.65));
        label->setLocalZOrder(10);
        label->setColor(Color3B(255,215,0));
        label->setTag(1102);
        this->addChild(label);

        auto labelCmt = Label::createWithTTF("YOU COLLECTED ALL WORMS","fonts/Mu online.ttf",32);
        labelCmt->setPosition(Point(visibleSize.width/2,visibleSize.height*0.65-label->getContentSize().height*1.5));
        labelCmt->setLocalZOrder(10);
        labelCmt->setTag(1103);
        this->addChild(labelCmt);
        auto labelNext = Label::createWithTTF("TOUCH ANY WHERE TO PLAY NEXT MAP","fonts/Mu online.ttf",32);
        labelNext->setPosition(Point(visibleSize.width/2,visibleSize.height*0.65-label->getContentSize().height*1.5-labelCmt->getContentSize().height));
        labelNext->setLocalZOrder(10);
        labelNext->setTag(1104);
        this->addChild(labelNext);
        timePlayed+=1;
        playTimeLabel->setString("Solved Map: "+intConvert(this->timePlayed)+"/"+intConvert(number>=15?10:1));
        if((number<15 && timePlayed==1)|| (number>=15 && timePlayed==10))
        {
            number=number<60?number+1:number;
            level+=1;
            timePlayed=0;
        }
        writeDataLevel();
        gameState=false;

        auto emitter=ParticleSystemQuad::create("fire_work.plist");
        emitter->setTextureWithRect(Director::getInstance()->getTextureCache()->addImage("fire_work.png"),Rect(0,0,54,62));
        emitter->runAction(Sequence::create(DelayTime::create(1.2),RemoveSelf::create(), NULL));
        addChild(emitter,11);
        return false;
    }
    return true;
}

void HelloWorld::loseCase()
{
    auto loseLayout = CSLoader::createNode("LoseLayer.csb"); //create Node
    this->addChild(loseLayout,4,1996); //add Node to Parent
    auto btnRetry = static_cast<ImageView*>(loseLayout->getChildByName("btn_retry"));
    auto btnExit = static_cast<ImageView*>(loseLayout->getChildByName("btn_exit"));
    if(btnRetry)
        btnRetry->addTouchEventListener(CC_CALLBACK_2(HelloWorld::touchBtnEvent, this));
    if(btnExit)
        btnExit->addTouchEventListener(CC_CALLBACK_2(HelloWorld::touchBtnEvent, this));
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
    "lose.wav");
}

void HelloWorld::touchBtnEvent(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
        {
        case cocos2d::ui::Widget::TouchEventType::BEGAN:
            if (((ImageView*)pSender)->getCallbackName() == "onRetryTouched")
            {
                getChildByTag(1996)->removeFromParentAndCleanup(true);
                for(int i=0; i<16; i++)
                {
                    for(int j=0; j<9; j++)
                    {

                        auto oldBuble=getChildByTag(hashTag(i,j));
                        if(oldBuble!=nullptr)
                        {
                            oldBuble->stopAllActions();
                            oldBuble->removeFromParentAndCleanup(true);
                        }
                    }
                }
                createMap(this->number,true);
            }else if (((ImageView*)pSender)->getCallbackName() == "onExitTouched")
            {
                //Play another map
                getChildByTag(1996)->removeFromParentAndCleanup(true);
                for(int i=0; i<16; i++)
                {
                    for(int j=0; j<9; j++)
                    {

                        auto oldBuble=getChildByTag(hashTag(i,j));
                        if(oldBuble!=nullptr)
                        {
                            oldBuble->stopAllActions();
                            oldBuble->removeFromParentAndCleanup(true);
                        }
                    }
                }
                createMap(this->number,false);
            }else if(((Button*)pSender)->getCallbackName()=="onExitGuide")
            {
                getChildByTag(2016)->removeFromParentAndCleanup(true);
            }
            break;
        case cocos2d::ui::Widget::TouchEventType::MOVED:
            break;
        case cocos2d::ui::Widget::TouchEventType::ENDED:
            break;
        case cocos2d::ui::Widget::TouchEventType::CANCELED:
            break;
        }
}


void HelloWorld::readDataLevel()
{
    this->level=UserDefault::getInstance()->getIntegerForKey("level",1);
    this->number=UserDefault::getInstance()->getIntegerForKey("number",5);
    this->timePlayed=UserDefault::getInstance()->getIntegerForKey("play_time",0);
}

void HelloWorld::writeDataLevel()
{
    UserDefault::getInstance()->setIntegerForKey("level",this->level);
    UserDefault::getInstance()->setIntegerForKey("number",this->number);
    UserDefault::getInstance()->setIntegerForKey("play_time",this->timePlayed);
}

void HelloWorld::saveMap()
{
    tmpHeroX=mg.heroX;
    tmpHeroY=mg.heroY;
    for(int i=0; i<16; i++)
    {
        for(int j=0; j<9; j++)
        {
            tmpMap[i][j]=mg.map[i][j];
        }
    }
}

void HelloWorld::restoreMap()
{
    mg.heroX=tmpHeroX;
    mg.heroY=tmpHeroY;
    for(int i=0; i<16; i++)
    {
        for(int j=0; j<9; j++)
        {
            mg.map[i][j]=tmpMap[i][j];
        }
    }
}

void HelloWorld::labelInit()
{
    levelLabel=Label::createWithTTF("Level: "+intConvert(this->level)+"/"+intConvert(number>=10?10:1),"fonts/Marker Felt.ttf",30);
    levelLabel->setTextColor(Color4B(255,255,255,255));
    levelLabel->setAnchorPoint(Vec2(0.0f,0.0f));
    levelLabel->setPosition(Point(visibleSize.width*0.05,visibleSize.height*0.07));
    playTimeLabel=Label::createWithTTF("Solved Map: "+intConvert(this->timePlayed),"fonts/Marker Felt.ttf",30);
    playTimeLabel->setAnchorPoint(Vec2(1.0f,0.0f));
    playTimeLabel->setTextColor(Color4B(255,255,255,255));
    playTimeLabel->setPosition(Point(visibleSize.width*0.95,visibleSize.height*0.07));
    guideLabel=Label::createWithTTF("SWIPE TO PLAY","fonts/Marker Felt.ttf",30);
    guideLabel->setAnchorPoint(Vec2(0.5f,0.5f));
    guideLabel->setTextColor(Color4B(075,000,130,127));
    guideLabel->setPosition(Point(visibleSize.width*0.50,visibleSize.height*0.07));
    this->addChild(levelLabel);
    this->addChild(playTimeLabel);
    this->addChild(guideLabel);
}

void HelloWorld::showGuide()
{
    auto guideLayout = CSLoader::createNode("Scene.csb"); //create Node
    this->addChild(guideLayout,4,2016); //add Node to Parent
    auto btnCloseGuide = static_cast<Button*>(guideLayout->getChildByName("btn_exit_guide"));
    if(btnCloseGuide)
        btnCloseGuide->addTouchEventListener(CC_CALLBACK_2(HelloWorld::touchBtnEvent, this));
}

bool HelloWorld::onContactBegin(const PhysicsContact& contact)
{
    CCLOG("LOG: COLLISION");
    auto tmpA = (Sprite*)contact.getShapeA()->getBody()->getNode();
    auto tmpB = (Sprite*)contact.getShapeB()->getBody()->getNode();
    if( (tmpA->getTag()==hashTag(mg.heroX,mg.heroY) && tmpB->getTag()==2011)
            || tmpB->getTag()==hashTag(mg.heroX,mg.heroY) && tmpA->getTag()==2011)
    {
        CCLOG("LOG: COLLISION- RUN ACTION");
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
        "land_jump.wav");
        stompWormAnimation=Animation::createWithSpriteFrames(stompWormFrames, 0.05f);
        FiniteTimeAction* fadeAction=Repeat::create( Animate::create(stompWormAnimation),1);
        getChildByTag(hashTag(mg.heroX,mg.heroY))->getPhysicsBody()->setContactTestBitmask(0);
        getChildByTag(hashTag(mg.heroX,mg.heroY))->stopAllActions();
        getChildByTag(hashTag(mg.heroX,mg.heroY))->runAction(fadeAction);
    }
}

void HelloWorld::ToastMessage(std::string msg)
{
    auto toastLabel=Label::createWithTTF(msg,"fonts/Marker Felt.ttf",20);
    toastLabel->setTextColor(Color4B(255,255,255,255));
    toastLabel->setPosition(Point(visibleSize.width*0.5,visibleSize.height*0.1));
    this->addChild(toastLabel,30);
    auto fadeOut = FadeOut::create(1.2f);
    auto sequence = Sequence::create(fadeOut, RemoveSelf::create() , NULL);
    toastLabel->runAction(sequence);
}

void HelloWorld::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
        clickTime+=1;
        if(clickTime == 1)
        {
            ToastMessage("Press one more time to exit game");
        }else if(clickTime == 2)
        {
            Director::getInstance()->end();
        }
}
