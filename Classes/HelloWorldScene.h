#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "ui/UIButton.h"
#include "ui/UIImageView.h"
#include "cocos2d.h"
#include "MapGenerator.h"
#include "EggMonster.h"
#include "AdmobHelper.h"
#include "SimpleAudioEngine.h"
#include "MyBodyParser.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "editor-support/cocostudio/ActionTimeline/CSLoader.h"
#include "ui/CocosGUI.h"
#include <sstream>
#include <string>
#include <iostream>
USING_NS_CC;

class HelloWorld : public cocos2d::Layer
{
private:
    bool gameState; //TRUE: Playing, FALSE: SOLVED
    int number, tmpNum, level, timePlayed;
    bool lockSwipe;
    int tmpHeroX, tmpHeroY, tmpMap[30][30];
    int newPos=-1, clickTime=0;
    float scaleRate;
    float firstX, firstY, sizeOfSquare, actualHeight;
    Node* background;
    Vec2 origin;
    Size visibleSize;
    EggMonster* eggMonster;
    MapGenerator mg;    
    bool isIdle;
    void createMonster(int tag,Vec2 position);
    //Animation
    SpriteBatchNode* wormBatch;
    SpriteFrameCache* wormCache;
    Animation* idleWormAnimation;
    Animation* stompWormAnimation;
    //frame vectors
    Vector<SpriteFrame* > idleWormFrames;
    Vector<SpriteFrame* > stompWormFrames;
    void initAnimation();
    //end of animaiton
    int remainWorm;
    bool isLose();
    void readDataLevel();
    void writeDataLevel();
    void saveMap();
    void restoreMap();
    Label* levelLabel;
    Label* playTimeLabel;
    Label* guideLabel;
    void labelInit();
    std::string intConvert(int input)
    {
        std::ostringstream ss;
        ss << input;
        return ss.str();
    }

    void ToastMessage(std::string);

public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    void createMap(int number,bool restore);
    int searchPath(int direct);
    void loseCase();
    void showGuide();
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

    // touch events
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event);

    void touchBtnEvent(cocos2d::Ref *pSender, ui::Widget::TouchEventType type);

    bool isTouchDown;

    float initialTouchPos[2];
    float currentTouchPos[2];
    int hashTag(int x, int y)
    {
        return ((x+y)*(x+y+1)/2+y);
    }
    void update(float dt);
    bool onContactBegin(const PhysicsContact& contact);
    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
};

#endif // __HELLOWORLD_SCENE_H__
