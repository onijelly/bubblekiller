#include "HelloWorldScene.h"

#include "SplashScreen.h"

USING_NS_CC;



Scene* SplashScreen::scene()
{
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    SplashScreen *layer = SplashScreen::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;


}


bool SplashScreen::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();

    Sprite* pSprite = Sprite::create("oni.png");

    // position the sprite on the center of the screen
    pSprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    pSprite->setScale(visibleSize.width/pSprite->getContentSize().width);
    // add the sprite as a child to this layer

    //bishnu-> you can control the time of the screen by changing 0.5s in the below to any number u wish.
    //instead directly this->runAction(CCSequence....) you can break it also into two parts.
    this->addChild(pSprite, 0);
    this->runAction( Sequence::create(
                                DelayTime::create(3),
                                CallFunc::create(this,
                                callfunc_selector(SplashScreen::loadSplash)),
                                 NULL));
    return true;
}

void SplashScreen::loadSplash()
 {
      Director::getInstance()->replaceScene(HelloWorld::createScene());
 }
