#include "EggMonster.h"

USING_NS_CC;

const std::string EggMonster::EGG_SS_PLIST="egg_monster.plist";
const std::string EggMonster::EGG_SS_PNG="egg_monster.png";
const int EggMonster::ACTION_TAG = 1993;
const int EggMonster::IDLE_TAG=2205;
SpriteBatchNode* EggMonster::spritebatch;
SpriteFrameCache* EggMonster::cache;

EggMonster::EggMonster()
{
}


EggMonster::~EggMonster()
{
}
EggMonster* EggMonster::create()
{
    EggMonster::spritebatch=SpriteBatchNode::create(EggMonster::EGG_SS_PNG);
    EggMonster::cache=SpriteFrameCache::getInstance();
    EggMonster::cache->addSpriteFramesWithFile(EggMonster::EGG_SS_PLIST);
    EggMonster* sprite=new EggMonster();
    if(sprite && sprite->initWithSpriteFrameName("egg_idle1.png"))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void EggMonster::initEverything()
{
    char str[100]={0};
    for(int i=1; i<=6; i++){
        sprintf(str,"egg_idle%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        idleFrames.pushBack(frame);
    }

    //first idle
    sprintf(str,"egg_idle1.png");
    //right frames
    SpriteFrame* firstIdleFrame=EggMonster::cache->getSpriteFrameByName(str);
    rightFrames.pushBack(firstIdleFrame);
    for(int i=1; i<=5; i++)
    {
        sprintf(str,"egg_right_rotate%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        rightFrames.pushBack(frame);
    }

    //left frames
    leftFrames.pushBack(firstIdleFrame);
    for(int i=1; i<=5; i++)
    {
        sprintf(str,"egg_left_rotate%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        leftFrames.pushBack(frame);
    }

    //down frames
    downFrames.pushBack(firstIdleFrame);
    for(int i=0; i<=4; i++)
    {
        sprintf(str,"down%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        downFrames.pushBack(frame);
    }

    //rotate back
    backFrames.pushBack(firstIdleFrame);
    for(int i=1; i<=7; i++)
    {
        sprintf(str,"egg_right_rotate%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        backFrames.pushBack(frame);
    }

    /*
     * Update 2016-10-19: Add animation when they idle START
     */
    for(int i=2; i<=5; i++)
    {
        sprintf(str,"egg_right_rotate%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        rightIdleFrames.pushBack(frame);
    }
    rightIdle = Animation::createWithSpriteFrames(rightIdleFrames,0.2f);

    for(int i=2; i<=5; i++)
    {
        sprintf(str,"egg_left_rotate%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        leftIdleFrames.pushBack(frame);
    }
    leftIdle = Animation::createWithSpriteFrames(leftIdleFrames,0.2f);

    for(int i=6; i<=7; i++)
    {
        sprintf(str,"egg_right_rotate%d.png",i);
        SpriteFrame* frame=EggMonster::cache->getSpriteFrameByName(str);
        upIdleFrames.pushBack(frame);
    }
    upIdle = Animation::createWithSpriteFrames(upIdleFrames,0.2f);
    /*
     * Update 2016-10-19: Add animation when they idle END
     */

    idleAnimation=Animation::createWithSpriteFrames(idleFrames,0.2f);
    leftAnimation=Animation::createWithSpriteFrames(leftFrames,0.1f);
    rightAnimation=Animation::createWithSpriteFrames(rightFrames,0.1f);
    backAnimation=Animation::createWithSpriteFrames(backFrames,0.1f);
    downAnimation=Animation::createWithSpriteFrames(downFrames,0.1f);

    rightAction=Repeat::create( Animate::create(rightAnimation),1);
    rightAction->retain();
    leftAction=Repeat::create( Animate::create(leftAnimation),1);
    leftAction->retain();
    downAction=Repeat::create( Animate::create(backAnimation),1);
    downAction->retain();
    backAction=Repeat::create( Animate::create(downAnimation),1);
    backAction->retain();

    idleUpAction = Repeat::create( Animate::create(upIdle),1);
    idleUpAction->retain();
    idleRightAction = Repeat::create( Animate::create(rightIdle),1);
    idleRightAction->retain();
    idleLeftAction = Repeat::create( Animate::create(leftIdle),1);
    idleLeftAction->retain();
    currentDirection=StepGenerator::DOWN;
}

void EggMonster::runIdle()
{
    stopAllActions();
    auto idleAction=RepeatForever::create( Animate::create(idleAnimation));
    idleAction->retain();
    runAction(idleAction);
}

void EggMonster::runRight(Vec2 des)
{
    stopAllActions();
    FiniteTimeAction* moveBy1 = MoveTo::create(0.25, des);
    //seq=Sequence::create(rightAction,moveBy1,rightAction->reverse(),nullptr);
    seq=Sequence::create(rightAction,moveBy1,nullptr);
    seq->setTag(ACTION_TAG);
    runAction(seq);
}

void EggMonster::runLeft(Vec2 des)
{
    stopAllActions();
    FiniteTimeAction* moveBy1 = MoveTo::create(0.25, des);
    //FiniteTimeAction* moveBy2 = MoveTo::create(0.25, des);
    //seq=Sequence::create(leftAction,moveBy1,leftAction->reverse(),nullptr);
    seq=Sequence::create(leftAction,moveBy1,nullptr);
    seq->setTag(ACTION_TAG);
    runAction(seq);
}

void EggMonster::runBack(Vec2 des)
{
    stopAllActions();
    FiniteTimeAction* moveBy1 = MoveTo::create(0.25, des);
    //seq=Sequence::create(backAction,moveBy1,backAction->reverse(),nullptr);
    seq=Sequence::create(backAction,moveBy1,nullptr);
    seq->setTag(ACTION_TAG);
    runAction(seq);
}

void EggMonster::runDown(Vec2 des)
{
    stopAllActions();
    FiniteTimeAction* moveBy1 = MoveTo::create(0.25, des);
    //seq=Sequence::create(downAction,moveBy1,downAction->reverse(),nullptr);
    seq=Sequence::create(downAction,moveBy1,nullptr);
    seq->setTag(ACTION_TAG);
    runAction(seq);
}

void EggMonster::idleRightRun()
{
    stopAllActions();
    runAction(idleRightAction);
}

void EggMonster::idleLeftRun()
{
    stopAllActions();
    runAction(idleLeftAction);
}

void EggMonster::idleUpRun()
{
    stopAllActions();
    runAction(idleUpAction);
}

void EggMonster::currentIdle()
{
    switch(currentDirection)
    {
    case StepGenerator::UP:
        runIdle();
        break;
    case StepGenerator::DOWN:
        idleUpRun();
        break;
    case StepGenerator::LEFT:
        idleLeftRun();
        break;
    case StepGenerator::RIGHT:
        idleRightRun();
        break;
    }
}
