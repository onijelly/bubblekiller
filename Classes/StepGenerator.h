#ifndef __STEP_GENERATOR_H__
#define __STEP_GENERATOR_H__
#include <random>
#include <time.h>
#include <sys/time.h>

class StepGenerator
{
private:
	int direct, distance, maxX, maxY;
public:
    static const int UP = 0;
    static const int DOWN = 1;
    static const int LEFT = 2;
    static const int RIGHT = 3;
	StepGenerator();
	StepGenerator(int argMaxX, int argMaxY);
	void nextStep(int currentX, int currentY);
	int getDistance();
	int getDirect();
	~StepGenerator();
};

#endif
