#ifndef __MAP_GENERATOR_H__
#define __MAP_GENERATOR_H__

#include "StepGenerator.h"
#include <queue>

class MapGenerator
{
private:
	int maxX;
	int maxY;
	int number;
	int currentX;
	int currentY;
    int previous;
	StepGenerator genStep;
public:
    int heroX, heroY;
    int map[30][30];
    std::queue<int> directions;
	MapGenerator();
	MapGenerator(int x, int y, int number);
	void genMap();
	int checkExist(int x, int y);
	//int checkInterrupt(int direct, int distance);
	int checkRow(int p1, int p2, int c);
	int checkCol(int p1, int p2, int r);
    int getMaxX()
    {
        return maxX;
    }

    int getMaxY()
    {
        return maxY;
    }

	~MapGenerator();
};

#endif // __HELLOWORLD_SCENE_H__
